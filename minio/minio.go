package minio

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"time"

	"github.com/minio/minio-go/v7"

	"github.com/phuslu/log"
)

var (
	ErrClientOffline  = errors.New("client offline")
	ErrObjectNotFound = errors.New("object not found")
)

const bucketName = "examplebucket"

// Minio Represents a Minio instance with a client.
type Minio struct {
	Logger      *log.Logger
	MinioClient *minio.Client
}

// NewMinio instanciates a new Minio struct.
func NewMinio(logger *log.Logger, client *minio.Client) (*Minio, error) {
	logger.Debug().Msg("initialize Minio")

	if client.IsOffline() {
		return nil, ErrClientOffline
	}

	// Create a default bucket that will be used for the entire session
	bucketExists, err := client.BucketExists(context.Background(), bucketName)
	if err != nil {
		return nil, fmt.Errorf("can't check if bucket exists: %w", err)
	}

	if !bucketExists {
		err := client.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{})
		if err != nil {
			return nil, fmt.Errorf("can't make new bucket: %w", err)
		}

		logger.Info().Str("Minio", client.EndpointURL().Host).Msg("Creating a new bucket")
	}

	return &Minio{
		Logger:      logger,
		MinioClient: client,
	}, nil
}

// GetObject uses the Minio client to request an object from an online Minio instance.
func (m *Minio) GetObject(objectID string) ([]byte, error) {
	// Timeout if the client does not respond
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	// Try to find is object exists
	_, err := m.MinioClient.StatObject(ctx, bucketName, objectID, minio.StatObjectOptions{})
	if err != nil {
		if errors.Is(err, context.DeadlineExceeded) {
			return nil, ErrClientOffline
		}

		return nil, ErrObjectNotFound
	}

	// The object should exists so get it and return it, otherwise return an error
	object, err := m.MinioClient.GetObject(context.Background(), bucketName, objectID, minio.GetObjectOptions{})
	if err != nil {
		return nil, fmt.Errorf("can't get object %s: %w", objectID, err)
	}

	// Read the object as string and return it
	body, err := ioutil.ReadAll(object)
	if err != nil {
		return []byte(""), fmt.Errorf("can't read object: %w", err)
	}

	return body, nil
}

// PutObject uses the Minio client to send an object to an online Minio instance.
func (m *Minio) PutObject(objectID string, reader io.Reader, contentLength int64) error {
	// Timeout if the client does not respond
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	_, err := m.MinioClient.PutObject(ctx, bucketName, objectID, reader, contentLength, minio.PutObjectOptions{})
	if err != nil {
		if errors.Is(err, context.DeadlineExceeded) {
			return ErrClientOffline
		}

		return fmt.Errorf("can't put object %s: %w", objectID, err)
	}

	return nil
}

// Keys return the list of keys stored in minio instance.
func (m *Minio) Keys() ([]string, error) {
	// Timeout if the client does not respond
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	objects := m.MinioClient.ListObjects(ctx, bucketName, minio.ListObjectsOptions{})

	// check channel here
	keys := make([]string, 0, len(objects))

	for object := range objects {
		if err := object.Err; err != nil {
			return nil, err
		}

		keys = append(keys, object.Key)
	}

	return keys, nil
}
