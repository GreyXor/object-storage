# build environment: https://hub.docker.com/_/golang
FROM golang:1.18 as build-env

WORKDIR /go/src/app

COPY ./ ./

RUN go build -ldflags="-s -w" -o /go/bin/object-storage

# running environment: https://hub.docker.com/_/docker
FROM docker:20.10

COPY --from=build-env /go/bin/object-storage /

EXPOSE 3000

ENTRYPOINT ["/object-storage"]
