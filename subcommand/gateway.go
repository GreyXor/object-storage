// Package subcommand implements commands that can be started by the user.
package subcommand

import (
	"fmt"

	"github.com/greyxor/object-storage/pool"
	"github.com/greyxor/object-storage/server"
	"github.com/phuslu/log"
)

// Gateway is a subcommand that instantiate a new Minio pool and starts an HTTP server to relays to it.
type Gateway struct {
	Logger *log.Logger
	Pool   *pool.Pool
	Server *server.Server
}

// NewGateway initializes a new gateway subcommand and ensure that the child variables are also properly initialized.
func NewGateway(logger *log.Logger) (*Gateway, error) {
	logger.Debug().Msg("initialize gateway subcommand")

	// Creation of a minio container pool
	pool, err := pool.NewPool(logger)
	if err != nil {
		return nil, fmt.Errorf("can't initialize pool: %w", err)
	}

	// Fill the pool with containers
	err = pool.MinioRefresh()
	if err != nil {
		return nil, fmt.Errorf("can't refresh minio client: %w", err)
	}

	// Instantiation of the HTTP server
	server := server.NewServer(logger, pool)

	return &Gateway{
		Logger: logger,
		Pool:   pool,
		Server: server,
	}, nil
}

// Start Starts the http server. This method stops when the server receives the interrupt signal.
func (g *Gateway) Start() error {
	g.Logger.Debug().Msg("starting gateway subcommand")

	// Enter the blocking loop of the main HTTP server thread.
	g.Server.Start()
	// ... Until the interrupt signal

	if err := g.stop(); err != nil {
		return fmt.Errorf("can't close gateway: %w", err)
	}

	return nil
}

// stop cleans up child variables and makes sure everything is properly closed.
func (g *Gateway) stop() error {
	g.Logger.Debug().Msg("stopping gateway subcommand")

	// HTTP server is self-cleaning

	if err := g.Pool.Stop(); err != nil {
		return fmt.Errorf("can't close pool: %w", err)
	}

	return nil
}
