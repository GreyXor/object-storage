package server

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/greyxor/object-storage/minio"
)

// GetMinioRefreshHandler asks the pool to regenerate the list of minio clients.
func (s *Server) GetMinioRefreshHandler(writer http.ResponseWriter, request *http.Request) {
	if err := s.Pool.MinioRefresh(); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
	}

	writer.Write([]byte("The minio pool has been refreshed. " + strconv.Itoa(len(s.Pool.Minios)) + " Minio found."))
}

// GetObjectHandler requests an object from the pool and returns it to the user.
func (s *Server) GetObjectHandler(writer http.ResponseWriter, request *http.Request) {
	// Get ID from query string
	objectID, err := s.fetchObjectID(writer, request)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte(err.Error()))

		return
	}

	object, err := s.Pool.FindFile(objectID)
	// Checks if the object exists or if the Minio client responds
	if err != nil {
		if errors.Is(err, minio.ErrObjectNotFound) {
			// If object isn't found. 404 Error
			writer.WriteHeader(http.StatusNotFound)
			writer.Write([]byte(err.Error()))

			return
		}

		// If there is any other error, we return it
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))

		return
	}

	writer.Write(object)
}

// PutObjectHandler puts an object in the pool.
func (s *Server) PutObjectHandler(writer http.ResponseWriter, request *http.Request) {
	// Get ID from query string
	objectID, err := s.fetchObjectID(writer, request)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte(err.Error()))

		return
	}

	minioID, err := s.Pool.UploadFile(objectID, request.Body, request.ContentLength)
	if err != nil {
		// Any other error, we return it
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))

		return
	}

	writer.Write([]byte("The object " + objectID + " has been written in the Minio " + strconv.Itoa(minioID)))
}

func (s *Server) GetKeys(writer http.ResponseWriter, request *http.Request) {
	keys, err := s.Pool.Keys()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)

		return
	}

	marshal, err := json.Marshal(keys)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)

		return
	}

	writer.Write(marshal)
}
