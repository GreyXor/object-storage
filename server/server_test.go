package server

import (
	"testing"
)

// TestDockerClient tests if docker API is able to negotiate version.
func TestValidID(t *testing.T) {
	t.Parallel()

	tests := map[string]bool{
		"":                                     false,
		"deadbeef":                             true,
		"deаdbeef":                             false,
		"234879876SDFSDFSDF":                   true,
		"!":                                    false,
		"FOOBARFOOBARFOOBARFOOBARFOOBARFOOBAR": false,
		"123456789":                            true,
	}

	for test, value := range tests {
		if valid := validID(test); valid != value {
			t.Fatalf(`ID %s should be %t, got: %t`, test, value, valid)
		}
	}
}
