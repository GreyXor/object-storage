package server

import (
	"context"
	"errors"
	"net"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/greyxor/object-storage/pool"
	"github.com/phuslu/log"
)

const (
	defaultTimeout = 15 * time.Second
	serverPort     = "3000"
)

var ErrInvalidID = errors.New("object ID is invalid. The IDs are alphanumeric, up to 32 characters")

// Server represents and stores everything related to the HTTP server.
type Server struct {
	Logger     *log.Logger
	Pool       *pool.Pool
	HTTPServer *http.Server
}

// NewServer initialise a new Server with muxer as well and check that all values are nominal.
func NewServer(logger *log.Logger, pool *pool.Pool) *Server {
	server := &Server{
		Logger: logger,
		Pool:   pool,
	}

	router := mux.NewRouter()
	router.HandleFunc("/minio/refresh", server.GetMinioRefreshHandler).Methods("GET")
	router.HandleFunc("/object/{id}", server.GetObjectHandler).Methods("GET")
	router.HandleFunc("/object/{id}", server.PutObjectHandler).Methods("PUT")
	router.HandleFunc("/keys", server.GetKeys).Methods("GET")

	httpServer := &http.Server{
		Handler:      router,
		Addr:         net.JoinHostPort("", serverPort),
		WriteTimeout: defaultTimeout,
		ReadTimeout:  defaultTimeout,
	}

	server.HTTPServer = httpServer

	return server
}

// Start starts the server and opens a new go routine that waits for the server to graceful shutdown it.
func (s *Server) Start() {
	idleConnsClosed := make(chan struct{})

	// Waits for an interrupt to shutdown the server
	go func() {
		// The expected signals to turn off the server (CTRL+C/syscall interrupt)
		ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
		defer stop()
		<-ctx.Done()

		// Does not accept any more requests, processes the remaining requests and stops the server
		s.Logger.Info().Msg("Requested shutdown in progress.. Press Ctrl+C again to force.")

		ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
		defer cancel()

		if err := s.HTTPServer.Shutdown(ctx); err != nil {
			s.Logger.Error().Err(err).Msg("The Server was forced to shutdown")
		}

		close(idleConnsClosed)
	}()

	s.Logger.Info().Msg("Server is now listening")

	// Start the server, the main thread will be blocked here
	if err := s.HTTPServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		s.Logger.Error().Err(err).Msg("HTTP Server can't listen and serve.")
		close(idleConnsClosed)
	}

	<-idleConnsClosed
}

// fetchObjectID Retrieve the ObjectID in the request and check that it is correct.
func (s *Server) fetchObjectID(writer http.ResponseWriter, request *http.Request) (string, error) {
	// Get the id from the query and if it's not good then we return an error
	objectID := mux.Vars(request)["id"]
	if !validID(objectID) {
		return "", ErrInvalidID
	}

	return objectID, nil
}

// The IDs are alphanumeric, up to 32 characters.
func validID(id string) bool {
	re := regexp.MustCompile(`(?m)^[a-zA-Z0-9]{1,32}$`)

	if !re.MatchString(id) {
		return false
	}

	return true
}
