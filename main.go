package main

import (
	"flag"
	"time"

	"github.com/greyxor/object-storage/subcommand"
	"github.com/phuslu/log"
)

func main() {
	// Initialization flags, logger and set debug mode settings
	debugFlag := flag.Bool("debug", false, "Show the debug logs")
	flag.Parse()

	logger := &log.Logger{
		Level:      log.InfoLevel,
		TimeFormat: time.Stamp,
		Writer: &log.ConsoleWriter{
			ColorOutput:    true,
			QuoteString:    true,
			EndWithMessage: false,
		},
	}

	if *debugFlag {
		logger.Level = log.DebugLevel
		logger.Debug().Msg("the log level is set to debug")
	}

	logger.Info().Msg("Starting Object-Storage")

	// As we only have one command for the moment, this one starts automatically.
	logger.Debug().Msg("gateway is the default subcommand and will be automatically started")

	// Try to initialize and start the gateway subcommand.
	if gatewayCommand, err := subcommand.NewGateway(logger); err != nil {
		logger.Error().Err(err).Msg("can't initialize gateway subcommand")
	} else {
		if err := gatewayCommand.Start(); err != nil {
			logger.Error().Err(err).Msg("can't start gateway subcommand")
		}
	}

	logger.Info().Msg("Object-Storage will now stop️")
}
