package pool

import (
	"errors"
	"fmt"
	"hash/crc32"
	"io"
	"net"
	"strings"
	"sync"

	"github.com/greyxor/object-storage/minio"

	"github.com/docker/docker/api/types/filters"
	"github.com/greyxor/object-storage/docker"
	"github.com/minio/minio-go/v7/pkg/credentials"

	"github.com/phuslu/log"

	minioL "github.com/minio/minio-go/v7"
)

const minioDefaultPort = "9000"

var ErrNoMiniosAV = errors.New("no minios available")

// Manage the Docker client and the Minio containers list.
type Pool struct {
	Logger *log.Logger
	Docker *docker.Docker
	Minios []*minio.Minio
}

// NewPool instanciates a new containers pool struct.
func NewPool(logger *log.Logger) (*Pool, error) {
	logger.Debug().Msg("initialize pool")

	docker, err := docker.NewDocker(logger)
	if err != nil {
		return nil, fmt.Errorf("can't initialize docker: %w", err)
	}

	return &Pool{
		Logger: logger,
		Docker: docker,
	}, nil
}

// MinioRefresh search in all containers, those which are usable Minio and keep them in Minios.
func (p *Pool) MinioRefresh() error {
	p.Logger.Debug().Msg("Retrieving Minios..")

	containersFilter := filters.NewArgs()

	// Ensures that only useful containers are collected
	containersFilter.Add("name", "amazin-object-storage-node")
	containersFilter.Add("ancestor", "minio/minio")
	containersFilter.Add("status", "running")
	containersFilter.Add("expose", minioDefaultPort)

	containers, err := p.Docker.FindContainersWithArgs(containersFilter)
	if err != nil {
		return fmt.Errorf("can't refresh minios: %w", err)
	}

	p.Logger.Debug().Int("Containers", len(containers)).Msg("Collected containers")
	p.Minios = make([]*minio.Minio, 0, len(containers))

	// Check all the containers and if they are good we add them to the pool
	for _, container := range containers {
		p.Logger.Debug().Str("Name", container.Names[0]).Msg("New Minio")

		containerJSONInspect, err := p.Docker.GetJSONInspectFromContainer(container.ID)
		if err != nil {
			return fmt.Errorf("can't retrieve env from container: %w", err)
		}

		minioAccessKey := getEnv(containerJSONInspect.Config.Env, "MINIO_ACCESS_KEY")
		minioSecretKey := getEnv(containerJSONInspect.Config.Env, "MINIO_SECRET_KEY")

		// If the container does not have the environment variables to connect, then it is not used
		if len(minioAccessKey) == 0 || len(minioSecretKey) == 0 {
			p.Logger.Warn().Str("container ID", container.ID).Msg("Minio container discovered but without the required environment variables")

			continue
		}

		clientHostPort := net.JoinHostPort(container.NetworkSettings.Networks[containerJSONInspect.HostConfig.NetworkMode.NetworkName()].IPAddress, minioDefaultPort)

		// New minio client
		minioClient, _ := minioL.New(clientHostPort, &minioL.Options{
			Creds:  credentials.NewStaticV4(minioAccessKey, minioSecretKey, ""),
			Secure: false,
		})

		newMinio, err := minio.NewMinio(p.Logger, minioClient)
		if err != nil {
			p.Logger.Error().Err(err).Msg("There is a problem with this Minio instance. We will not use it. Please fix the problem on this instance and refresh the list")

			continue
		}

		// A new Minio for the pool
		p.Logger.Debug().Str("Name", container.Names[0]).Msg("New container used")
		p.Minios = append(p.Minios, newMinio)
	}

	p.Logger.Info().Int("Discovered", len(p.Minios)).Msg("Minio instances refresh")

	return nil
}

// Stop cleans up child variables and makes sure everything is properly closed.
func (p *Pool) Stop() error {
	p.Logger.Debug().Msg("closing docker")

	if err := p.Docker.Stop(); err != nil {
		return fmt.Errorf("can't close Docker: %w", err)
	}

	return nil
}

// LoadBallanceID allows to keep a relation between the id of an object and the id. We use here the crc32
// function to keep the unit of a string and transform it (more or less randomly) in the number of the Minio.
func (p *Pool) LoadBalanceID(objectID string) (int, error) {
	if countMinios := len(p.Minios); countMinios == 0 {
		p.MinioRefresh()
	}

	if countMinios := len(p.Minios); countMinios == 0 {
		return 0, ErrNoMiniosAV
	}

	// Using CRC-32 most common polynomial
	hasher := crc32.NewIEEE()

	// Transforms objectID to crc32 (Enough to have a unique identifier)
	_, err := hasher.Write([]byte(objectID))
	if err != nil {
		return 0, fmt.Errorf("can't write objectID to crc32 hasher: %w", err)
	}

	// The division allows here to have an adequate identifier with the number of Minio client

	return int(hasher.Sum32() % uint32(len(p.Minios))), nil
}

// FindFile Look for the corresponding minio id and ask for the object.
func (p *Pool) FindFile(objectID string) ([]byte, error) {
	minioID, err := p.LoadBalanceID(objectID)
	if err != nil {
		return nil, fmt.Errorf("can't load balance ID: %w", err)
	}

	object, err := p.Minios[minioID].GetObject(objectID)
	if err != nil {
		// It is not normal that the client is offline, we will refresh the list of Minio clients just in case
		if errors.Is(err, minio.ErrClientOffline) {
			p.MinioRefresh()
		}

		p.Logger.Warn().Str("Object ID", objectID).Int("Container ID", minioID).Msg("File not found")

		return nil, fmt.Errorf("can't get object from minio: %w", err)
	}

	p.Logger.Info().Str("Object ID", objectID).Int("Container ID", minioID).Msg("File found")

	return object, nil
}

// UploadFile Will put an object in a Minio.
func (p *Pool) UploadFile(objectID string, reader io.ReadCloser, length int64) (int, error) {
	minioID, err := p.LoadBalanceID(objectID)
	if err != nil {
		return 0, fmt.Errorf("can't load balance ID: %w", err)
	}

	err = p.Minios[minioID].PutObject(objectID, reader, length)
	if err != nil {
		// It is not normal that the client is offline, we will refresh the list of Minio clients just in case
		if errors.Is(err, minio.ErrClientOffline) {
			p.MinioRefresh()
		}

		return 0, fmt.Errorf("can't put object from minio: %w", err)
	}

	p.Logger.Info().Str("Object ID", objectID).Int("Container ID", minioID).Msg("Written file")

	return minioID, nil
}

// getEnv find a value from an environment variable.
func getEnv(envVariables []string, envNameWanted string) string {
	// Split the variables one after the other until you find the one that contains the desired variable
	for _, e := range envVariables {
		pair := strings.SplitN(e, "=", 2)
		if pair[0] == envNameWanted {
			return pair[1]
		}
	}

	// Or maybe regex matching with (^[A-Z0-9_]+)(\=)(.*\n((\-|\+)?=[A-Z])|.*$) ?

	return ""
}

// Keys return keys of every minio instances.
func (p *Pool) Keys() ([]string, error) {
	keys := make([]string, 0)

	wg := new(sync.WaitGroup)

	// Share every minio on go routines
	for _, minio := range p.Minios {
		wg.Add(1)

		go addKeysMinio(&keys, minio, wg)
	}

	wg.Wait()

	return keys, nil
}

// addKeysMinio add keys from m to keys.
func addKeysMinio(keys *[]string, m *minio.Minio, wg *sync.WaitGroup) {
	defer wg.Done()

	minioKeys, _ := m.Keys()

	*keys = append(*keys, minioKeys...)
}
