# Object-Storage ⚡️

- ✅ Dynamic amazin-object-storage-node containers with Image, Network filters...
- ✅ Stateless & consistent ID load balance
- ✅ HTTP listening on 3000 and with 2 endpoints (one more for manually reload Minio clients)
- ✅ The IDs are alphanumeric, up to 32 characters (otherwise user got error)
- ✅ Docker aware, timeout and live reload
- ✅ Error handling
- ✅ Graceful shutdown with interrupt
- ✅ A small CI and some tests
- ✅ Multithreaded objects listing

![Diagram](./diagram.png)

## Technical note

### Stateless
To have a stateless behavior and still keep an identification of Minio containers. I identify the id of the object as a unique string thanks to CRC32 and then divide it by the actually number of minio clients.
Depending on the use case, other solutions could be considered. The problem here is that if the number of Minio instances increases, then it can distort the result.

### PUT Handler : Rewrite object or return error
By referring to the [MDN documentation on the PUT verb](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT). I chose that PUT would rewrite the object without returning an error.

### Live Reload

Several mechanisms are present to add or remove Minio clients. A Minio client refresh is performed when :  :
- The application is started.
- If during a request a Minio client does not respond.
- Manually by the user by calling the `/minio/refresh` endpoint.


### OpenAPI

Adding an automatic OpenAPI generation was feasible but would add a lot of code. I thought that for an api of a few endpoints it was not so essential.
I still made an OpenAPI file that you can find in the docs folder

## Getting started
Prerequisite
- Docker & Docker-Compose
```shell
git clone git@github.com:GreyXor/object-storage.git && cd object-storage
docker-compose up --build
```

## Tests
`go test ./...`

## Standards
In order to improve interoperability, this application tries to get as close as possible to these different standards.
- [Semantic Versioning](https://semver.org/)
- [The twelve-factor app is a methodology for building software-as-a-service apps.](https://12factor.net/)
- [A specification for adding human and machine readable meaning to commit messages.](https://www.conventionalcommits.org/)
- [All dates and times comply with the ISO 8601 standard.](https://www.iso.org/iso-8601-date-and-time-format.html)
