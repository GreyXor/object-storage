package docker

import (
	"context"
	"fmt"
	"time"

	"github.com/docker/docker/api/types/filters"

	"github.com/docker/docker/api/types"

	"github.com/docker/docker/client"
	"github.com/phuslu/log"
)

// Docker allow to communicate with Docker on host.
type Docker struct {
	Logger    *log.Logger
	APIClient *client.Client
}

const apiClientTimeout = 30 * time.Second

// NewDocker instanciates a new Docker struct.
func NewDocker(logger *log.Logger) (*Docker, error) {
	logger.Debug().Msg("initialize docker")

	apiClient, err := client.NewClientWithOpts(client.FromEnv, client.WithTimeout(apiClientTimeout), client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, fmt.Errorf("can't initialize API client: %w", err)
	}

	logger.Debug().Str("client version", apiClient.ClientVersion()).Msg("API client initialized")

	return &Docker{
		Logger:    logger,
		APIClient: apiClient,
	}, nil
}

// Stop should be called before removing Docker object.
func (d *Docker) Stop() error {
	d.Logger.Debug().Msg("closing docker")

	if err := d.APIClient.Close(); err != nil {
		return fmt.Errorf("can't close API client: %w", err)
	}

	return nil
}

// FindContainersWithArgs Returns all containers corresponding to the filters.
func (d *Docker) FindContainersWithArgs(args filters.Args) ([]types.Container, error) {
	d.Logger.Debug().Strs("args", args.Keys()).Msg("Finding containers with args")

	// what is order ?
	containers, err := d.APIClient.ContainerList(context.Background(), types.ContainerListOptions{
		Filters: args,
	})
	if err != nil {
		return nil, fmt.Errorf("can't find containers: %w", err)
	}

	return containers, nil
}

// GetJSONInspectFromContainer get the container inspection json.
func (d *Docker) GetJSONInspectFromContainer(id string) (types.ContainerJSON, error) {
	containerJSON, err := d.APIClient.ContainerInspect(context.Background(), id)
	if err != nil {
		return types.ContainerJSON{}, fmt.Errorf("can't fetch container json: %w", err)
	}

	return containerJSON, nil
}
