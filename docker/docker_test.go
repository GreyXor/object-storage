package docker_test

import (
	"testing"

	"github.com/greyxor/object-storage/docker"
	"github.com/phuslu/log"
)

// TestDockerClient tests if docker API is able to negotiate version.
func TestDockerAPIClientNegotiateVersion(t *testing.T) {
	t.Parallel()

	docker, err := docker.NewDocker(&log.Logger{})
	if err != nil {
		t.Fatalf(`can't initialize Docker: %v`, err)
	}

	if cVersion := docker.APIClient.ClientVersion(); cVersion == "" {
		t.Fatalf(`API client can't negotiate version, got: %s`, cVersion)
	}
}
